%
% Description: Latex Template for Generic Document @ Ra�l Pe�a Ortiz
%
% Author: Ra�l Pe�a-Ortiz
% Creation date: 18/7/2010
%
% (c) 2010 Ra�l Pe�a-Ortiz
%              Todos los derechos reservados
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rpenya-document}[2010/07/18 Template for Generic Document]

\def\@DocTitle{\@title}

\def\@subtitle{}
 	
\newcommand{\subtitle}[1]{\gdef\@subtitle{#1}}

\def\@version{}
 	
\newcommand{\version}[1]{\gdef\@version{#1}}

\gdef\keywords#1{\def\pdfkeywords{#1}\hypersetup{pdfkeywords={#1}}}

\gdef\pdfauthor{AUTOR}
\gdef\hypersetup{pdfauthor={AUTOR}.}

\newif\if@spanishlanguage
\global\let\@spanishlanguage

\DeclareOption{spanish}{\@spanishlanguagetrue}

\DeclareOption*{\PassOptionsToClass{11pt,\CurrentOption}{article}}
\ProcessOptions
\LoadClass{report}

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{rpenya-style}

\usepackage{listings}
\usepackage{rpenya-style}

\setlength{\hoffset}{-2cm}
\setlength{\voffset}{-2.5cm}
\setlength{\textheight}{22cm}
\setlength{\textwidth}{17cm}
\setlength{\headheight}{2cm}
\setlength{\marginparwidth}{1cm}

\def\dash{\noalign{\vskip1.5pt}\hline\noalign{\vskip1.5pt}}

\pagestyle{headings}

\pagestyle{fancy}

\def\maketitle{%

\thispagestyle{empty}

\begin{picture}(0,0)(0,650)
\put(0,0){\makebox(0,0)[bl]{\textcolor{darkcolor}{\copyright \hspace{0.25cm}\number\the\year\hspace{0.25cm} Ra\'ul Pe\~na-Ortiz.}}}
\put(375,0){\@date}
\end{picture}


\bigskip 
\bigskip 
\bigskip 


\begin{center}
       \Huge{\bf \@DocTitle}\\
       \bigskip 
       \hrule
       \bigskip 
       \Large{\textcolor{darkcolor}{\bf \@subtitle}}\\
       \bigskip 
       \textcolor{greycolor}{{\tt \@version}}

\end{center}

\bigskip 
\bigskip 
\bigskip 

\bigskip 

\begin{center}
\@author
\end{center}

\newpage
}

\setcounter{page}{0}

\fancyhead{} % clear all header fields
\fancyhead[L]{}
\fancyhead[C]{}
\fancyhead[R]{\textcolor{darkcolor}{\bf \@DocTitle}}

\fancyfoot[C]{} 
\fancyfoot[L]{\textcolor{darkcolor}{\@subtitle}}
\fancyfoot[R]{\thepage/\pageref{LastPage}}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\newcommand{\ident}{\hspace*{0.5cm}}

\newcommand{\separav}{\vspace*{0.i25cm}}
\newcommand{\separah}{\hspace*{1cm}}

\hypersetup{
backref,%
bookmarks=true,%
bookmarksnumbered=true,%
colorlinks,%
citecolor=black,%
filecolor=black,%
linkcolor=black,%
urlcolor=black,%
pdftex}

\newcommand{\email}[1]{\href{mailto:#1}{#1}}

% CVPublications
% #1 Title of Group
% #2 BibTex Files
% #3 BibTex Style
\def\CVPublicationsGroup#1#2#3{
\renewcommand{\bibname}{#1}
\begin{bibunit}[#3]
\small
\nocite{*}
\putbib[#2]
\end{bibunit}
}